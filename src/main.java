import java.util.Scanner;

public class main{

    static double Percent(double total, double part){
        return ((part*100)/total);
    }

        public static void main(String[] args) {
            String title;
            String author;
            double pages;
            double progress;
            Scanner myObj = new Scanner(System.in);
            System.out.println("Create a reading list.");
            boolean more = true;
            boolean tracker = false;
            String ans;
            do {
                System.out.println("Start by entering the title of a book.");
                title = myObj.nextLine();
                System.out.println("Enter the authors name.");
                author = myObj.nextLine();
                System.out.println("how many pages is the book?");
                pages = myObj.nextDouble();
                Book a = new Book(title, author, pages);
                System.out.println("details about " + a.toString());

                do {
                    System.out.println("how many many pages have you read?");
                    progress = myObj.nextDouble();
                    if (progress <= a.getPages()) {
                        a.setProgress(progress);
                        tracker = true;
                    } else {
                        System.out.println("that's more pages then there are in the book. Please enter a number less than " + a.getPages());
                    }
                }while (!tracker);
                System.out.println("you have completed " + Percent(a.getPages(),a.getProgress())+"% of your book");
                Scanner input = new Scanner(System.in);
                System.out.println("do you have another book to add? Y/N ");
                ans = input.nextLine();
                if (ans.equals("N")) more = false;
            }while(more);

    }
}
