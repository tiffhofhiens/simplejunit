//https://www.youtube.com/watch?v=o5pE7L2tVV8&list=PL-suslzEBiMobIYkpy0ijFzSZF1w_ok8r

/**
 * Tiffany Quinlan
 * Date: 5/21/20
 * Week 5 JUnit testing
 */
public class Book {
    private String title;
    private String author;
    private double pages;
    private double progress;

    private boolean isFinished;

    public Book(String title, String author, double pages) {
        this.title = title;
        this.author = author;
        this.pages = pages;
        this.isFinished = false;
    }


    public String getTitle() {return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPages() {
        return pages;
    }

    public void setPages(double pages) {
        this.pages = pages;
    }

    public double getProgress() { return progress;}

    public void setProgress(double progress) {
        this.progress = progress;
        if (progress == this.getPages()){
            this.isFinished = true;
        }
    }

    public boolean isFinished() { return isFinished; }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", pages=" + pages +
                '}';
    }
}

