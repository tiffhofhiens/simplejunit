

import org.junit.jupiter.api.Test;
//import static org.hamcrest.CoreMatchers.*;
import static org.junit.jupiter.api.Assertions.*;

class BookTest {

    @Test
    void getTitle() {
        Book b = new Book("Turtle", "Tiffany", 100);
        assertEquals("Turtle", b.getTitle());
    }
    @Test
    void getAuthor() {
        Book b = new Book("Turtle", "Tiffany", 100);
        assertEquals("Tiffany", b.getAuthor());
    }
    @Test
    void getPages() {
        Book b = new Book("Turtle", "Tiffany", 100);
        assertNotNull(true, String.valueOf(b.getPages()));
    }
    @Test
    void setProgress() {
        Book b = new Book("Turtle", "Tiffany", 100);
        b.setProgress(50);
        assertTrue(b.getPages()>=b.getProgress());
    }
    @Test
    void getProgress() {
        Book b = new Book("Turtle", "Tiffany", 100);
        b.setProgress(50);
        assertFalse(b.getProgress()>=100, "wrong number of pages");
    }
//  Deprecated assertThat in latest junit library
//
//    @Test
//    void That() {
//        Book b = new Book("Turtle", "Tiffany", 100);
//        b.setProgress(100);
//        assertThat(true, Is(b.isFinished()));
//    }


    // additional tests
    // possible use cases array of books in reading list. check to see is percent is working correctly
    @Test
    void ArrayEquals() {
        int[] myNum = {10, 20, 30, 40};
        assertArrayEquals(myNum, myNum);
    }

    @Test
    void NotSame() {
        int[] myNum = {10, 20, 30, 40};
        int[] newNum = {10, 20, 30, 40};
        assertNotSame(false, myNum);
        assertNotSame(myNum, newNum);
        assertNotSame(newNum, myNum);
        assertNotSame("urNum", newNum);
    }

    @Test
    void Same() {
        int[] myNum = {10, 20, 30, 40};
        int[] newNum = myNum;
        assertSame(myNum, myNum);
        assertSame(myNum, newNum);
    }

    @Test
    void Null() {
        Book b = null;
        assertNull(b);
    }


}